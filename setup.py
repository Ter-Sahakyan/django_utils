from setuptools import find_packages
from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()


setup(
    name='django_utils',
    packages=find_packages(),
    version='0.0.1',
    install_requires=requirements,
)
