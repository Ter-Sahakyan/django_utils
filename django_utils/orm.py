from django.db import models
from django.apps import apps

from django.utils.text import capfirst


@classmethod
def model_field_exists(cls, field):
    try:
        cls._meta.get_field(field)
    except models.FieldDoesNotExist:
        return False
    else:
        return True

def verbose_name(app_model, field=None, cap_first=True):
    """Get verbose name of field or models"""
    if isinstance(app_model, str):
        opts = apps.get_model(app_model, require_ready=False)._meta
    else:
        opts = app_model._meta

    if field is not None:
        if isinstance(field, str):
            verbose_name = opts.get_field(field).verbose_name
        else:
            names = []
            for field_name in field:
                verbose_name = opts.get_field(field_name).verbose_name
                names.append(
                    str(
                        capfirst(verbose_name)
                        if cap_first else verbose_name,
                    ),
                )
            return names
    else:
        verbose_name = opts.verbose_name

    return capfirst(verbose_name) if cap_first else verbose_name
