from django.forms import BaseInlineFormSet


class AddRequestToFormMixin:

    def _define_class(self, FormKlass, request, obj=None, **kwargs):
        class FormWithRequest(FormKlass):

            def __new__(cls, *args, **kwargs):
                kwargs['request'] = request
                return FormKlass(*args, **kwargs)
        return FormWithRequest

    def get_form(self, request, obj=None, **kwargs):
        FormKlass = super().get_form(request, obj, **kwargs)
        return self._define_class(FormKlass, request, obj=None, **kwargs)

    def get_formset(self, request, obj=None, **kwargs):
        FormKlass = super().get_formset(request, obj, **kwargs)
        return self._define_class(FormKlass, request, obj=None, **kwargs)


class FormSetWithParentObjectMixin:
    """
        Adds parent models instance to modelform of admin inline
    """

    class FormSetWithParentObject(BaseInlineFormSet):
        def get_form_kwargs(self, index):
            kwargs = super().get_form_kwargs(index)
            kwargs['parent_object'] = self.instance
            return kwargs

    formset = FormSetWithParentObject

    # https://stackoverflow.com/questions/9422735/accessing-parent-model-instance-from-modelform-of-admin-inline