from copy import deepcopy
from typing import Any

from django.core.exceptions import ObjectDoesNotExist
from django.db import models


class ModelDiffMixin:
    """
    This mixin saves the history of fields values 1 step back
    It's useful while operation logging

    On initialization, it saves the old values to _original attr
    and then compares current values with the original ones to generate
    difference with old and new values set.
    """
    watch_fields = []
    _original = {}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._set_original_done = False
        self.is_new = False if self.pk else True
        # TODO: Remove this from the init model
        self.set_original()

    def get_fields(self):
        result = {}
        for watch_field in self.watch_fields:
            result.update({
                watch_field: self._get_fields(watch_field),
            })

        self._set_original_done = True
        return result

    def set_original(self):
        if not self._set_original_done:
            self._original = self.get_fields()

    def _get_fields(self, field_name):
        try:
            result = getattr(self, field_name)
        except ObjectDoesNotExist:
            result = None

        if isinstance(result, (dict, list)):
            result = deepcopy(result)

        return result

    def diff(self) -> dict[str, tuple[Any, Any]]:
        """
        Return dict with field_name as key and
        tuple (old_value, new_value) as value
        """
        if not self._set_original_done:
            self.set_original()

        result = {}
        current_model_fields = self.get_fields()
        for watch_field in self.watch_fields:
            if (
                current_model_fields.get(watch_field) !=
                self._original.get(watch_field)
            ):
                result.update({
                    watch_field: (
                        self._original.get(watch_field),
                        current_model_fields.get(watch_field),
                    ),
                })
        return result


# django_utils
class DiffModel(ModelDiffMixin, models.Model):

    class Meta:
        abstract = True
